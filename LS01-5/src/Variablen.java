
public class Variablen {

	public static void main(String[] args) {
		 /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
		short durchlaeufe;

  /* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		durchlaeufe=25;
		System.out.println("-----------------Durchlaufe-----------------");
		System.out.println(durchlaeufe);
				
  /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */
		char Eingabe;
		
  /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		Eingabe='C';
		System.out.println("-----------------Eingabe-----------------");
		System.out.println(Eingabe);
				
  /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
		double AstronomischeWerte;

  /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/
		AstronomischeWerte=3E5;
		System.out.println("-----------------AstronomischeWerte-----------------");
		System.out.println(AstronomischeWerte);

  /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/
		byte Mitglieder = 7;
		
  /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
		System.out.println("-----------------Mitglieder-----------------");
		System.out.println(Mitglieder);
		
  /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
		char Elementarladung= 'e';
		double Elementarladung2= 1.60217646E-19;
		System.out.println("-----------------Elementarladung-----------------");
		System.out.println(Elementarladung);
		System.out.println(Elementarladung2);
		
  /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
		boolean Zahlungerfolgt;

  /*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/
		Zahlungerfolgt=true;
		System.out.println("-----------------Zahlungerfolgt-----------------");
		System.out.println(Zahlungerfolgt);
	}

}
