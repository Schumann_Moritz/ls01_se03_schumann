
public class Aufgabe3 {

	public static void main(String[] args) {
		//AUFGABE 3 V.1
				String far1 = "-20";
				String far2 = "-10";
				String far3 = "+0";
				String far4 = "+20";
				String far5 = "+30";
				String cel1 = "-28.89";
				String cel2 = "-23.33";
				String cel3 = "-17.78";
				String cel4 = "-6.67";
				String cel5 = "-1.11";
				System.out.printf("\n----HIER IST DIE TABELLE AUS AUFGABE 3 v.1 !!!----\n%-12s|%10s", "Fahrenheit","Celsius");
				System.out.print("\n-----------------------");
				System.out.printf("\n%-12s|%10s", far1,cel1);
				System.out.printf("\n%-12s|%10s", far2,cel2);
				System.out.printf("\n%-12s|%10s", far3,cel3);
				System.out.printf("\n%-12s|%10s", far4,cel4);
				System.out.printf("\n%-12s|%10s", far5,cel5);
				
		//AUFGABE 3 V.1
				int a   = -20;
				int b   = -10;
				int c   = 0;
				int d   = 20;
				int e   = 30;
				double f = -28.8889;
				double g = -23.3333;
				double h = -17.7778;
				double i = -6.6667;
				double j = -1.1111;
				System.out.printf("\n----HIER IST DIE TABELLE AUS AUFGABE 3 v.2 !!!----\n%-12s|%10s", "Fahrenheit","Celsius");
				System.out.print("\n-----------------------");
				System.out.printf("\n%+-12d|%10.2f", a,f);
				System.out.printf("\n%+-12d|%10.2f", b,g);
				System.out.printf("\n%+-12d|%10.2f", c,h);
				System.out.printf("\n%+-12d|%10.2f", d,i);
				System.out.printf("\n%+-12d|%10.2f", e,j);
				
				
	}

}
