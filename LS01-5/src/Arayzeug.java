import java.util.Scanner;
import java.util.Random;

public class Arayzeug {

	public static void main(String[] args) {
		// aufgabe1();
		// aufgabe2();
		aufgabe4();
	}

	// AUFGABE 1.1
	public static void aufgabe1() {

		System.out.println("Aufgabe 1");

		int[] liste = new int[10];
		int x = 0;

		// AUFGABE

		for (int i = 0; i < 10; i++) {
			liste[i] = x;
			x = x + 1;
			// System.out.printf(x+ "|" +i+"\n");
		}
		// Ausgabe

		for (int i = 0; i < liste.length; i++) {
			System.out.print(liste[i] + " ");
		}
		System.out.println();
	}

	// ----------------------------------------------------------------------------------------------------------------------------------
	// AUFGABE 1.2
	public static void aufgabe2() {
		System.out.println("Aufgabe 2");

		int[] liste = new int[10];
		int y = 1;

		// AUFGABE

		for (int i = 0; i < 10; i++) {
			liste[i] = y;
			y = y + 2;
			// System.out.printf(y+ "|" +i+"\n");
		}
		// Ausgabe

		for (int i = 0; i < liste.length; i++) {
			System.out.print(liste[i] + " ");
		}
		System.out.println();
	}

	// ----------------------------------------------------------------------------------------------------------------------------------
	// Aufgabe 1.3

	public static void aufgabe3() {

		System.out.println("Aufgabe 3");

		Scanner input = new Scanner(System.in);
		char[] liste = new char[5];

		// AUFGABE

		for (int i = 0; i < 5; i++) {
			System.out.println("Geben sie Zahl " + (i + 1) + " ein :");
			liste[i] = input.next().charAt(0);
			System.out.println(liste[i]);
		}
		// Ausgabe

		for (int i = 0; i < liste.length; i++) {
			System.out.print(liste[i] + " ");
		}
		System.out.println();

		// Ausgabe R�CKW�RTS bzw. Umgekehrt

		for (int i = liste.length - 1; i >= 0; i = i--) { // i--
			System.out.print(liste[i] + " ");
		}
		System.out.println();
	}

	// -----------------------------------------------------------------------------------------------------------------------------------
	// Aufgabe 1.4
	public static void aufgabe4() {
		int[] liste = { 3, 7, 12, 18, 37, 42 };
		boolean zahlgefunden12 = false;
		boolean zahlgefunden13 = false;
		// int[] lliste = new int[6];
		// Random lzahl = new Random();
		// Zahlengenerator und nein der produziert keinen Strom
		// for (int i = 0; i < 6; i++) {
		// lliste[i] = lzahl.nextInt(49);
		// }
		System.out.print("[ ");
		for (int i = 0; i < liste.length; i++) {
			System.out.print(liste[i] + " ");
		}
		System.out.printf("]\n");

		for (int i = 0; i < liste.length - 1; i++) {
			if (liste[i] == 12) {
				zahlgefunden12 = true;
			}
			if (liste[i] == 13) {
				zahlgefunden13 = true;
			}
		}
		if (zahlgefunden12 == true) {
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");
		}
		if (zahlgefunden13 == true) {
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		}
	}
}
