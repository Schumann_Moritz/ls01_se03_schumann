
public class mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m;
      berechnung(4.0,2.0);
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      //m = (x + y) / 2.0;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      //System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
   public static double berechnung (double x, double y) {
	   double m= (x+y)/2;
	   return ausgabe (m,x,y);
   }
   public static double ausgabe (double m,double x, double y) {
	   System.out.print("Der Mittelwert von "+ x +" und "+ y +" ist "+ m);
	   return m;
   }
}

