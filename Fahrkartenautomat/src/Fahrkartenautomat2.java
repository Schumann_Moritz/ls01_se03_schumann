
import java.util.Scanner;

class Fahrkartenautomat2 {

    public static double fahrkartenbestellungErfassen() {
        int anzahlTickets;
        int ticket;
        double ticketpreis = 0.0;
        Scanner tastatur = new Scanner(System.in);

        System.out.printf("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\n	Einzelfahrschein Regeltarif AB 		[ 2,90 EUR] (1)\n	Tageskarte Regeltarif AB 		[ 8,60 EUR] (2)\n	Kleingruppen-Tageskarte Regeltarif AB 	[23,50 EUR] (3)\n");
        							
        ticket = tastatur.nextInt();
        
        if (ticket==1) {
        	ticketpreis=ticketpreis+2.9;
        }
        else if (ticket==2) {
        	ticketpreis=ticketpreis+8.6;
        }
        else if (ticket==3){
        	ticketpreis=ticketpreis+23.5;
        }
        else {
        	System.out.println("Diese auswahl gibt es nicht");
        	fahrkartenbestellungErfassen();
        }
        	
        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();

        return ticketpreis * anzahlTickets;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f EUR %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Der R�ckgabebetrag in H��he von %4.2f EUR %n", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen																																																																	lol
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Müzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
            
        }
    }

    public static void main(String[] args) {

        double zuZahlenderBetrag;
        double rueckgabebetrag;

        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(rueckgabebetrag);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir w�nschen Ihnen eine gute Fahrt.\n--------------------------------------------------------------\n\n");
    }
}
